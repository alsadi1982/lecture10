package ru.edu.logger;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Aspect
@Component
public class LoggerAspect {

    @Pointcut("execution(* ru.edu.MyController.*(..))")
    public void myControllerPointcut(){

    }

    /**
     * Method writes in console message about which method of MyController class is started and with which arguments
     * @param joinPoint
     */
    @Before("myControllerPointcut()")
    public void beforeMethod(JoinPoint joinPoint){
        String methodName = joinPoint.getSignature().getName();
        List<String> args =Arrays.stream(joinPoint.getArgs()).map(elem -> "arg = " + elem).collect(Collectors.toList());
        System.out.println(LocalDateTime.now() + " Method = " + methodName + " args = " + args + " started!" );

    }

    /**
     * Method writes in console message about which method of MyController class is finished and with which arguments
     * @param joinPoint
     */
    @AfterReturning("myControllerPointcut()")
    public  void afterMethod(JoinPoint joinPoint){
        String methodName = joinPoint.getSignature().getName();
        List<String> args =Arrays.stream(joinPoint.getArgs()).map(elem -> "arg = " + elem).collect(Collectors.toList());
        System.out.println(LocalDateTime.now() + " Method = " + methodName + " args = " + args + " finished!" );

    }

    /**
     * Method writes in console message about which method of MyController class is threw exception and what exception was throwing
     * @param joinPoint
     * @param ex - threw exception
     */
    @AfterThrowing(value = "myControllerPointcut()", throwing = "ex")
    public void afterException(JoinPoint joinPoint, Throwable ex){
        String methodName = joinPoint.getSignature().getName();
        List<String> args =Arrays.stream(joinPoint.getArgs()).map(elem -> "arg = " + elem).collect(Collectors.toList());
        String message = String.format(" Method = %s args = %s ERROR!!! %s", methodName, args, ex.getMessage());
        System.out.println(LocalDateTime.now() + " " + message);
    }
}
