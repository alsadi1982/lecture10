package ru.edu;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.servlet.ModelAndView;
import ru.edu.db.Contribution;
import ru.edu.db.ContributionsDao;

import java.util.Collection;

@RestController
@RequestMapping(value = "/info", consumes = MediaType.ALL_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class MyController {

    private ContributionsDao dao;

    @Autowired
    public void setDao(ContributionsDao dao) {
        this.dao = dao;
    }


    /**
     * Method MyController#getAllDeposits() handles GET request and renders jsp page
     * @return ModelAndView object
     */
    @GetMapping
    public ModelAndView getAllDeposits(){

        Collection<Contribution> list = dao.getAllContributions();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/deposits.jsp");
        modelAndView.addObject("url",  dao.getLocalUrl());
        modelAndView.addObject("depositHolder",  dao.getDepositHolder());
        modelAndView.addObject("deposits",  list);
        return modelAndView;
    }

    /**
     * Method MyController#getDepositById(int id) handles GET request
     * @param id of deposit which we want to get from request param
     * @return message with result of GET request
     */
    @GetMapping(value = "/get")
    public String getDepositById(@RequestParam("depositId") int id){
        Contribution contribution = dao.getContributionById(id);
        return "Deposit with ID: " + id + " is: " + contribution.toString();
    }

    /**
     * Method MyController#createDeposit(String request) handles POST request
     * @param request - request string from request body
     * @return message with result
     */
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public String createDeposit(@RequestBody String request){

        JSONObject jsonObject = new JSONObject(request);
        String name = jsonObject.getString("name");
        String moneyQuantity = jsonObject.getString("moneyQuantity");
        String term = jsonObject.getString("term");

        Contribution contribution = new Contribution(name, moneyQuantity, term);
        dao.createContribution(contribution);

        return "Deposit successful created!";
    }

    /**
     * Method MyController#deleteDeposit(String request) handles DELETE request
     * @param request - request string from request body
     * @return message with result
     */
    @DeleteMapping
    public String deleteDeposit(@RequestBody String request){

        dao.deleteContributionByName(request);
        String message = "Deposit with name: " + request + "successful deleted!";

        return message;
    }

    /**
     * Method MyController#updateDeposit(String request) handles PUT request
     * @param request - request string from request body
     * @return message with result
     */
    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public String updateDeposit(@RequestBody String request){

        JSONObject jsonObject = new JSONObject(request);
        String name = jsonObject.getString("name");
        String moneyQuantity = jsonObject.getString("moneyQuantity");
        String term = jsonObject.getString("term");
        int id = jsonObject.getInt("id");

        dao.updateContributionById(id, name, moneyQuantity, term);

        return "Deposit with ID: " + id + " successful updated!";
    }
}
