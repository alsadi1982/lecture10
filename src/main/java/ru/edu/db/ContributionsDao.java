package ru.edu.db;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;

@Component
@PropertySource("classpath:db.properties")
public class ContributionsDao {

    private String localUrl;
    private String depositHolder;

    public String getDepositHolder() {
        return depositHolder;
    }

    @Value("${deposit.holder}")
    public void setDepositHolder(String depositHolder) {
        this.depositHolder = depositHolder;
    }


    @Value("${local.url}")
    public void setLocalUrl(String localUrl) {
        this.localUrl = localUrl;
    }

    public String getLocalUrl() {
        return localUrl;
    }


    /**
     * Method ContributionsDao#getAllContributions() returned collection of contributions
     * @return List of contributions
     */
    public Collection<Contribution> getAllContributions(){

        final String getAllContributionsQuery = "Select * from contributions";
        Collection<Contribution> list = new ArrayList<>();

            try (Connection connection = DriverManager.getConnection(localUrl);
                 Statement statement = connection.createStatement();
                 ResultSet resultSet = statement.executeQuery(getAllContributionsQuery)) {

                while (resultSet.next()) {
                    Contribution contribution = new Contribution(resultSet.getString("name"),
                            resultSet.getString("money_quantity"),
                            resultSet.getString("term"));
                    contribution.setId(resultSet.getInt("id"));
                    list.add(contribution);
                }
            } catch (SQLException ex) {
                ex.getStackTrace();
            }

        return list;
    }

    /**
     * Method ContributionsDao#createContribution(Contribution contribution) insert new contribution into table contributions
     * @param contribution - contribution for insertion
     * @return true - if contribution inserted, false - if not
     */
    public boolean createContribution(Contribution contribution){

        final String createPersonRequest = "insert into contributions (name, money_quantity, term) values(?,?,?)";
        int affectedRows = 0;

            try (Connection connection = DriverManager.getConnection(localUrl);
                 PreparedStatement preparedStatement = connection.prepareStatement(createPersonRequest)) {

                preparedStatement.setString(1, contribution.getName());
                preparedStatement.setString(2, contribution.getMoneyQuantity());
                preparedStatement.setString(3, contribution.getTerm());
                affectedRows = preparedStatement.executeUpdate();

            } catch (SQLException ex) {
                System.out.println(ex.getSQLState());
            }

        return affectedRows > 0;
    }

    /**
     * Method ContributionsDao#getContributionById(int id) get contribution by ID
     * @param id - unique contribution ID
     * @return contribution with this id
     * @throws  IllegalArgumentException if contribution doesn't exist
     */
    public Contribution getContributionById(int id){

        final String getPersonByNameQuery = "Select * from contributions where id = ?";
        Contribution contribution = null;

            try (Connection connection = DriverManager.getConnection(localUrl);
                 PreparedStatement preparedStatement = connection.prepareStatement(getPersonByNameQuery)) {

                preparedStatement.setInt(1, id);
                ResultSet resultSet = preparedStatement.executeQuery();
                contribution = new Contribution(resultSet.getString("name"),
                        resultSet.getString("money_quantity"),
                        resultSet.getString("term"));
                contribution.setId(resultSet.getInt("id"));
                resultSet.close();

            } catch (SQLException ex) {
                ex.getStackTrace();
            }


        if (contribution == null){
            throw new IllegalArgumentException("Contribution with id = " + id + " doesn't exist!");
        }

        return contribution;
    }

    /**
     * Method ContributionsDao#deleteContributionByName(String name) delete contribution from table Users by name
     * @param name - contribution's name which we want to delete
     * @return true - if we deleted contribution
     * @throws IllegalArgumentException if contribution not deleted
     */
    public boolean deleteContributionByName(String name){

        final String deletePersonQuery = "delete from contributions where name = ?";
        int affectedRows = 0;


            try (Connection connection = DriverManager.getConnection(localUrl);
                 PreparedStatement preparedStatement = connection.prepareStatement(deletePersonQuery)) {

                preparedStatement.setString(1, name);
                affectedRows = preparedStatement.executeUpdate();

            } catch (SQLException ex) {
                ex.printStackTrace();
            }


        if (affectedRows == 0){
            throw new IllegalArgumentException("Contribution not found!");
        }

        return affectedRows > 0;
    }

    /**
     * Method ContributionsDao#updateContributionById(int id, String name, String money_quantity, String term) update contribution with given ID
     * @param id  which we want to update
     * @param name - new contribution's name
     * @param money_quantity - new contribution's money quantity
     * @param term - new contirbution's term
     * @return true -if we updated contribution, false - if not
     */
    public boolean updateContributionById(int id, String name, String money_quantity, String term){

        getContributionById(id);

        final String updatePersonByIdQuery = "update contributions set name = ?, money_quantity = ?, term = ? where id = ?";
        int affectedRows = 0;

            try (Connection connection = DriverManager.getConnection(localUrl);
                 PreparedStatement preparedStatement = connection.prepareStatement(updatePersonByIdQuery)) {

                preparedStatement.setString(1, name);
                preparedStatement.setString(2, money_quantity);
                preparedStatement.setString(3, term);
                preparedStatement.setInt(4, id);
                affectedRows = preparedStatement.executeUpdate();

            } catch (SQLException ex) {
                ex.printStackTrace();
            }

        return affectedRows > 0;

    }
}
