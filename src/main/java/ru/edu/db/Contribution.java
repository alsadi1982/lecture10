package ru.edu.db;

import java.util.Objects;

public class Contribution {

    private int id;
    private String name;
    private String moneyQuantity;
    private String term;

    public Contribution(String name, String moneyQuantity, String term) {
        this.name = name;
        this.moneyQuantity = moneyQuantity;
        this.term = term;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMoneyQuantity() {
        return moneyQuantity;
    }

    public void setMoneyQuantity(String moneyQuantity) {
        this.moneyQuantity = moneyQuantity;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) return false;
        if (this == other) return true;
        if (!this.getClass().equals(other.getClass())) return false;
        Contribution that = (Contribution) other;
        return this.id == that.id && this.name.equalsIgnoreCase(that.name) && this.moneyQuantity.equalsIgnoreCase(that.moneyQuantity) && this.term.equalsIgnoreCase(that.term);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getMoneyQuantity(), getTerm());
    }

    @Override
    public String toString() {
        return "Contribution{" +
                " name='" + name + '\'' +
                ", moneyQuantity='" + moneyQuantity + '\'' +
                ", term='" + term + '\'' +
                '}';
    }
}
