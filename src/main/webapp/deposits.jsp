<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8" %>

<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
    <h2>Deposit holder: ${depositHolder}</h2>
    <h1>All deposits:</h1>
    <table>
        <tr>
            <th>ID</th>
            <th>Название вклада</th>
            <th>Сумма вклада</th>
            <th>Срок вклада</th>
        </tr>
        <c:forEach items="${deposits}" var="deposit">
        <tr>
            <td>${deposit.id}</td>
            <td>${deposit.name}</td>
            <td>${deposit.moneyQuantity}</td>
            <td>${deposit.term}</td>
        </tr>
        </c:forEach>
    </table>
    </body>
</html>