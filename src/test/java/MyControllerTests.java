import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.edu.MyController;
import ru.edu.db.Contribution;
import ru.edu.db.ContributionsDao;

public class MyControllerTests {

    /**
     * Checking the successful execution MyController#getDepositById(int id)
     */
    @Test
    public void getDepositById_Successful_Test (){

        MyController myController = new MyController();
        int id = 7;
        Contribution contribution = new Contribution("NEWDeposit", "20000$", "2 years");

        ContributionsDao dao = Mockito.mock(ContributionsDao.class);
        Mockito.when(dao.getContributionById(id)).thenReturn(contribution);
        myController.setDao(dao);

        String expected = "Deposit with ID: " + id + " is: " + contribution.toString();
        String actual = myController.getDepositById(id);

        Assertions.assertEquals(expected, actual);

    }

    /**
     * Checking the unsuccessful execution MyController#getDepositById(int id)
     */
    @Test
    public void getDepositById_Fail_Test (){

        MyController myController = new MyController();
        int id = 7;

        ContributionsDao dao = Mockito.mock(ContributionsDao.class);
        Mockito.when(dao.getContributionById(id)).thenThrow(IllegalArgumentException.class);
        myController.setDao(dao);

        Assertions.assertThrows(IllegalArgumentException.class, () -> myController.getDepositById(id));
    }

    /**
     * Checking the successful execution MyController#createDeposit(String request)
     */
    @Test
    public void createDeposit_Successful_Test (){

        MyController myController = new MyController();

        ContributionsDao dao = Mockito.mock(ContributionsDao.class);
        myController.setDao(dao);

        String expected = "Deposit successful created!";
        String actual = myController.createDeposit("{\n" +
                "   \"name\": \"NEWDeposit\",\n" +
                "   \"moneyQuantity\": \"200000$\",\n" +
                "   \"term\": \"2 years\"\n" +
                "}");

        Assertions.assertEquals(expected, actual);
    }

    /**
     * Checking the unsuccessful execution MyController#createDeposit(String request)
     */
    @Test
    public void createDeposit_Fail_Test (){

        MyController myController = new MyController();

        ContributionsDao dao = Mockito.mock(ContributionsDao.class);
        myController.setDao(dao);

        String request = "Hello world";

        Assertions.assertThrows(org.json.JSONException.class, () -> myController.createDeposit(request));
    }

    /**
     * Checking the successful execution MyController#gdeleteDeposit(String request)
     */
    @Test
    public void deleteDeposit_Successful_Test (){

        MyController myController = new MyController();
        String request = "NEWDeposit";

        ContributionsDao dao = Mockito.mock(ContributionsDao.class);
        Mockito.when(dao.deleteContributionByName(request)).thenReturn(true);
        myController.setDao(dao);

        String expected = "Deposit with name: " + request + "successful deleted!";
        String actual = myController.deleteDeposit(request);

        Assertions.assertEquals(expected, actual);

    }

    /**
     * Checking the unsuccessful execution MyController#deleteDeposit(String request)
     */
    @Test
    public void deleteDeposit_Fail_Test (){

        MyController myController = new MyController();
        String request = "NEWDeposit";

        ContributionsDao dao = Mockito.mock(ContributionsDao.class);
        Mockito.when(dao.deleteContributionByName(request)).thenThrow(IllegalArgumentException.class);
        myController.setDao(dao);

        Assertions.assertThrows(IllegalArgumentException.class, () -> myController.deleteDeposit(request));
    }

    /**
     * Checking the successful execution MyController#updateDeposit(String request)
     */
    @Test
    public void updateDeposit_Successful_Test (){

        MyController myController = new MyController();

        int id = 7;
        String request = "{\n" +
                "\t\"id\": " + id + ",\n" +
                "   \"name\": \"NEWDeposit\",\n" +
                "   \"moneyQuantity\": \"2000$\",\n" +
                "   \"term\": \"6 month\"\n" +
                "}";

        ContributionsDao dao = Mockito.mock(ContributionsDao.class);
        Mockito.when(dao.updateContributionById(id, "NEWDeposit","2000$", "6 month" )).thenReturn(true);
        myController.setDao(dao);

        String expected = "Deposit with ID: " + id +" successful updated!";
        String actual = myController.updateDeposit(request);

        Assertions.assertEquals(expected, actual);

    }

    /**
     * Checking the unsuccessful execution MyController#updateDeposit(String request)
     */
    @Test
    public void updateDeposit_Fail_Test (){

        MyController myController = new MyController();
        String request = "{\n" +
                "\t\"id\": 20,\n" +
                "   \"name\": \"NEWDeposit\",\n" +
                "   \"moneyQuantity\": \"2000$\",\n" +
                "   \"term\": \"6 month\"\n" +
                "}";

        ContributionsDao dao = Mockito.mock(ContributionsDao.class);
        Mockito.when(dao.updateContributionById(20, "NEWDeposit","2000$", "6 month" )).thenThrow(IllegalArgumentException.class);
        myController.setDao(dao);

        Assertions.assertThrows(IllegalArgumentException.class, () -> myController.updateDeposit(request));
    }
}
